# **Praktikum Modul 1 Sistem Operasi**
Berikut adalah Repository dari Kelompok A10 untuk pengerjaan Praktikum Modul 1 Sistem Operasi. Dalam Repository ini terdapat Anggota Kelompok, Dokumentasi serta Penjelasan tiap soal, _Screenshot_ Output, dan Kendala yang Dialami. 

# **Anggota Kelompok**
| Nama                      | NRP        | Kelas            |
| ------------------------- | ---------- | ---------------- |
| Anas Azhar                | 5025211043 | Sistem Operasi A |
| Dian Lies Seviona Dabukke | 5025211080 | Sistem Operasi A |
| Akmal Ariq Romadhon       | 5025211188 | Sistem Operasi A |

# **Dokumentasi dan Penjelasan Soal**
Berikut adalah dokumentasi yang berisi source code dari tiap soal dan penjelasan terkait perintah yang digunakan. 
## **Soal Nomor 1**

Bocchi hendak melakukan University Admission Test di Jepang. Bocchi ingin masuk ke universitas yang bagus. Akan tetapi, dia masih bingung sehingga ia memerlukan beberapa strategi untuk melakukan hal tersebut. Untung saja Bocchi menemukan file .csv yang berisi ranking universitas dunia untuk melakukan penataan strategi :

- **Soal Nomor 1A :** <br>
  Bocchi ingin masuk ke universitas yang bagus di Jepang. Oleh karena itu, Bocchi perlu melakukan survey terlebih dahulu. Tampilkan 5 Universitas dengan ranking tertinggi di Jepang.

**Penyelesaian Soal 1A :**\
Dalam menyelesaikan soal 1A, digunakan*syntax* AWK. Berikut adalah code yang digunakan dalam menyelesaikan soal 1A

```bash
awk -F, '$4 == "Japan" {print $1 " " $2 " " $4}' 2023\ QS\ World\ University\ Rankings.csv | head -n 5
```

Dalam code tersebut, `-F,` berfungsi membentuk kolom dari CSV dengan value yang dipisahkan tanda koma. Kemudian pada `$4 == "Japan` dilakukan sorting pada kolom nomor 4 yang memiliki value `Japan`. Selanjutnya dilakukan print pada kolom 1, 2, dan 4 dengan _syntax_ `{print $1 " " $2 " " $4}`. Selanjutnya, simbol pipe atau `|` digunakan untuk melanjutkan argument yang sebelumnya. Terakhir, bagian `head -n 5` digunakan untuk meenampilkan 5 universitas terbaik yang berada di Jepang. Tidak perlu dilakukan sorting lagi dalam kolon `rank` karena secara _default,_ data pada CSV tersebut telah urut berdasarkan ranking.
<br>

- **Soal Nomor 1B :** <br>
  Karena Bocchi kurang percaya diri dengan kemampuannya, coba cari Faculty Student Score(fsr score) yang paling rendah diantara 5 Universitas di Jepang.

**Penyelesaian Soal 1B :**\
Dalam soal 1B, _syntax_ yang digunakan secara garis besar masih sama, yaitu AWK. Berikut adalah code yang digunakan dalam menyelesaikan soal 1B

```bash
awk -F, '$4 == "Japan" {print}' 2023\ QS\ World\ University\ Rankings.csv | sort -t, -g -k9 | head -n 5 | awk -F, '{print $2}'
```

Perbedaan code pada soal 1B jika dibandingkan dengan soal 1A terletak pada bagian `sort -t, -k9`. Dalam bagian tersebut, bagian `t,` berfungsi untuk memisahkan kolom pada file csv. Bagian`k9` digunakan untuk sorting kolom ke-9 yang berisi skor FRS dari tiap universitas. Selanjutnya, pada bagian `head -n 1` digunakan untuk menampilkan universitas dengan skor FSR paling rendah. Terakhir bagian `awk -F, '{print $2}` berfungsi untuk print nama universitas yang telah di sorting.

- **Soal Nomor 1C :** <br>
  Karena Bocchi takut salah membaca ketika memasukkan nama universitas, cari 10 Universitas di Jepang dengan Employment Outcome Rank(ger rank) paling tinggi.

**Penyelesaian Soal 1C :**\
Dalam soal 1C, _syntax_ yang digunakan secara garis besar masih sama dengan soal-soal sebelumnya. Berikut adalah code yang digunakan dalam menyelesaikan soal 1B

```bash
awk -F, '$4 == "Japan" {print $2 "," $20}' 2023\ QS\ World\ University\ Rankings.csv | sort -t, -k2 -n | head -n 10 | awk -F, '{print $1 " " $2}'
```

Perbedaan code pada soal 1C jika dibandingkan dengan soal 1B terletak pada bagian ` sort -t, -k2 -g`. Dalam bagian tersebut, bagian `t,` berfungsi untuk memisahkan kolom `$2` dan `$20` yang telah dibuat pada bagian sebelumnya menjadi kolom 1 dan 2. Kemudian bagian`k2` digunakan untuk sorting kolom ke-2 dengen `-n` atau sorting untuk jenis variabel `numerik`. Selanjutnya, pada bagian `head -n 10` digunakan untuk menampilkan 10 universitas dengan skor Employmenr Outcome Rank (Ger) paling tinggi.

- **Soal Nomor 1D :** <br> 
Bocchi ngefans berat dengan universitas paling keren di dunia. Bantu bocchi mencari universitas tersebut dengan kata kunci keren.

**Penyelesaian Soal 1D :**\
Dalam soal 1D, _syntax_ yang digunakan telah berbeda namun lebih sederhana. Berikut adalah code yang digunakan untuk menyelesaikan soal nomor 1D

```bash
awk -F, '/Keren/ {print $2}' 2023\ QS\ World\ University\ Rankings.csv
```

Dalam code tersebut, hanya digunakan `awk -F` dan `/Keren/` untuk mencari _keyword_ Keren dalam file CSV. Terakhir, digunakan `{print $2}` untuk menampilkan nama universitasnya.
<br>
<br>

## **Soal Nomor 2**

Kobeni ingin pergi ke negara terbaik di dunia bernama Indonesia. Akan tetapi karena uang Kobeni habis untuk beli headphone ATH-R70x, Kobeni tidak dapat melakukan hal tersebut.

**Penyelesaian Soal 2 :**
<br>
Untuk menyelesaikan soal tersebut, code yang digunakan ialah sebagai berikut.

```bash
#!/bin/bash

cd $(pwd)
crontab -l > joblist
echo "0 */10 * * * cd $(pwd) && bash fordownload.sh" >> joblist
echo "0 0 * * * cd $(pwd) && bash forzipping.sh" >> joblist
crontab joblist

# Mencari nomor folder yang belum digunakan
echo -e '#!/bin/bash/'
cd $(pwd)
i=1
while [ -d "kumpulan_$i" ]; do
    (( i++ ))
done

# Membuat folder baru dengan nama "kumpulan_N"
foldername="kumpulan_$i"
mkdir "$foldername"

# Mendapatkan jam sekarang
hour=$(date +%H)

# Menghitung jumlah download yang akan dilakukan (X)
if [ $hour -eq 0 ]; then
    X=1
else
    X=$hour
fi

# Loop untuk melakukan download sebanyak X kali
for (( j=1; j<=$X; j++ ))
do
    # Membuat nama file dengan format "perjalanan_NOMOR.FILE"
    filename="perjalanan_$j.jpg"

    # Melakukan download gambar dari URL yang diinginkan
    wget -O "$foldername/$filename" https://source.unsplash.com/1080x1920/?indonesia
done'>fordownload.sh

chmod +x fordownload.sh

echo -e '#!/bin/bash'
cd $(pwd)
foldernum=$(ls -d kumpulan_* 2>/dev/null | wc -l)
zipfolder=$(ls -d Devil_* 2>/dev/null | wc -l)
zipname="Devil_$(($zipfolder+1)).zip"
for(( i=1; i<=$foldernum; i++ ))
do
 zip -r $zipname kumpulan_$i
 rm -r kumpulan_$i
done'>forzipping.sh

chmod +x forzipping.sh
```

Dalam code tersebut, terdapat beberapa bagian. Bagian pertama berfungsi untuk mengatur `cronjob` seperti yang ditugaskan pada soal. Berikut ialah code yang digunakan

```bash
cd $(pwd)
crontab -l > joblist
echo "0 */10 * * * cd $(pwd) && bash fordownload.sh" >> joblist
echo "0 0 * * * cd $(pwd) && bash forzipping.sh" >> joblist
crontab joblist
```

Bagian `cd $(pwd)` berfungsi untuk memindahkan direktori sesuai letak code. Kmemudian terdapat fungsi `crontab-l > joblist` untuk memindahkn cronjob kedalam file `joblist`. Selanjutnya ada fungsi `echo "0 */10 * * * cd $(pwd) && bash fordownload.sh" >> joblist` untuk menjalankan file `fordownload` tiap 10 jam sekali. Dalam fungsi tersebut, digunakan fungsi `cd $(pwd)` untuk memindahkan directory sesuai lokasi folder, kemudian dimasukk kedalam file `joblist`. Selanjutnya `echo "0 0 * * * cd $(pwd) && bash forzipping.sh" >> joblist` digunakan untuk menjalankan file `forzipping.sh` tiap jam 12 malam. File `forzipping` nantinya akan dimasukkan kedalam `joblist` untuk dieksesuki dengan fungsi `crontab joblist`

Selanjutnya, terdapat code yang digunakan untuk mencari nomor folder kumpulan yang belum digunakan.

```bash
# Mencari nomor folder yang belum digunakan
echo -e '#!/bin/bash/'
cd $(pwd)
i=1
while [ -d "kumpulan_$i" ]; do
    (( i++ ))
done
```

bagian `echo -e` digunakan untuk menyalin isi dari code tersebut kedalam file baru bernama `fordownload`. kemudian digunakan file `cd $(pwd)` untuk mengarahkan path kedalam folder yang sama. Kemudian terdapat deklarasi variabel `i=1` yang digunakan pada perulangan while. Dalam perulangan tersebut, digunakan _syntax_ `-d` untuk menghitung jumlah folder yang ada. Dan apabila telah terdapat folder `kumpulan_i`, maka i nya akan terus bertambah atau dilakukan _increment._

Setelah melakukan perulangan untuk mencari nomor folder yang digunakan, code dilanjutkan ke bagian selanjutnya.

```bash
#Membuat folder baru dengan nama "kumpulan_N"
foldername="kumpulan_$i"
mkdir "$foldername"
```

Bagian tersebut berfungsi untuk deklarasi variabel `foldername` yang diberi nama `kumpulan_$i`. Selanjutnya digunakan _syntax_ `mkdir $foldername` untuk pembuatan folder sesuai `variabel i` yang telah dideklarasi sebelumnya.

Setelah dibuat folder `kumpulan_$i`, code dilanjutkan untuk menentukan jam saat code dijalankan. Berikut adalah code yang digunakan.

```bash
# Mendapatkan jam sekarang
hour=$(date +%H)
```

Dalam bagian tersebut, hanya dilakukan deklarasi `variabel hour` yang diisi dengan _syntax_ `$(date +%H)` untuk melihat atau mendeteksi jam saat code tersebut dijalankan. Bagian tersebut akan menentukan jumlah gambar yang akan _didonwnload_ sesuai dengan perintah pada soal. 

Setelah mendapatkan jam saat user menjalankan code, proses akan dilanjutkan kedalam _if else_ untuk memenuhi perintah pada soal yaitu apabila code dijalankan pada jam 00:00, maka gambar yang _didownload_ adalah 1. Berikut adalah code yang digunakan.
```bash
if [ $hour -eq 0 ]; then
    X=1
else
    X=$hour
fi
``` 
Dalam fungsi tersebut, if pertama memuat kemungkinan apabila `$hour = 1`, maka `variabel x` bernilai 1. Kemudian apabila `$hour` tidak sama dengan 1, maka nilai dari `variabel x` akan mengikut `$hour`.

Kemudian proses kembali dilanjutkan untuk _mendownload_ gambar. Berikut adalah code yang digunakan.
```bash
# Loop untuk melakukan download sebanyak X kali
for (( j=1; j<=$X; j++ ))
do
    # Membuat nama file dengan format "perjalanan_NOMOR.FILE"
    filename="perjalanan_$j.jpg"

    # Melakukan download gambar dari URL yang diinginkan
    wget -O "$foldername/$filename" https://source.unsplash.com/1080x1920/?indonesia
done'>fordownload.sh
```
Dalam bagian tersebut, code pertama yang akan dijalankan ialah looping dari satu hingga kurang dari sama dengan jam yang diberikan. Kemudian dilakuka deklarasi `variabel filename` dengan nama `perjalanan_$j` yang nantinya akan membuat file perjalanan yang dimulai dari 1 hingga jumlah jam saat code dijalankan. Selanjutnya, digunakan _syntax_ wget untuk _mendownload_ gambar, dengan _directory_ menyesuaikan dengan variabel yang telah dideklarasi sebelumnya. Terakhir, seluruh perintah atau code sebelumnya akan dimasukkan kedalam file `fordownload.sh` yang nantinya akan dimasukkan kedalam crontab untuk dieksekusi.

Setelah file fordownload dibuat, file tersebut akan ditambah _permission_ agar dapat dijalankan aatau dieksekusi oleh _user_. Berikut adalah code yang digunakan.
```bash
chmod +x fordownload.sh
```
_syntax_ tesebut membuat file tetap bisa dijalankan oleh crontab, bahkan ketika _user_ sedang AFK (Away From Keyboard).

Setelah seluruh proses sebelumnya dijalankan, maka code akan dilanjutkan ke proses akhir, yaitu zip file. Berikut adalah code yang digunakan.
```bash
echo -e '#!/bin/bash'
cd $(pwd)
foldernum=$(ls -d kumpulan_* 2>/dev/null | wc -l)
zipfolder=$(ls -d Devil_* 2>/dev/null | wc -l)
zipname="Devil_$(($zipfolder+1)).zip"
for(( i=1; i<=$foldernum; i++ ))
do
 zip -r $zipname kumpulan_$i
 rm -r kumpulan_$i
done'>forzipping.sh

chmod +x forzipping.sh
```
Dalam code tersebut, terdapat perintah `echo -e` untuk menyalin bagian tersebut yang nantinya akan dimasukkan kedalam file `forzipping.sh`. Terdapat _syntax_ `cd $(pwd)` yang berarti memindahkan direktori ke direktori dimana code ini dijalankan. _Syntax_ ini memungkinkan seluruh _user_ yang memiliki code ini dapat mengeksekusi code dimanapun directorynya.
<br><br>
Selanjutnya, terdapat deklarasi `variabel zipfolder` yang bertujuan untuk menentukan nomor urut untuk file zip yang akan dibuat. Script akan mencari apakah ada file zip dengan nama "Devil_" di awalnya pada direktori kerja saat ini, menggunakan perintah ls -d Devil_*, lalu menghitung jumlah file zip tersebut dengan wc -l. Jumlah file zip yang ditemukan akan disimpan ke dalam variabel zipfolder. Berikut adalah code yang digunakan.
```bash
zipfolder=$(ls -d Devil_* 2>/dev/null | wc -l)
```

Proses dilanjutkan dengan deklarasi `variabel zipname` yangbertujuan untuk menentukan nama file zip yang baru. Nama file zip baru ini akan dimulai dengan "Devil_" diikuti dengan nomor urut yang ditemukan pada baris sebelumnya `($zipfolder+1)`, dan diakhiri dengan ekstensi ".zip". Nama file zip baru ini akan disimpan ke dalam variabel zipname. Berikut adalah code yang digunakan.
```bash
zipname="Devil_$(($zipfolder+1)).zip"
```

Kemudian terdapat perulangan atau _looping_ dalam code tersebut yang berfungsi memulai sebuah loop for yang akan mengulang sebanyak jumlah dari `foldernum`. 
Lalu terdapat _syntax_ `zip -r $zipname kumpulan_$i` yang akan mengkompresi folder dengan nama `"kumpulan_$i"` ke dalam file zip dengan nama yang telah ditentukan sebelumnya pada variabel zipname. Terakhir, terdapat _syntax _ `rm -r kumpulan_$i` yang digunakan untuk menghapus folder yang telah berhasil dikompresi, perintah rm akan dijalankan untuk menghapus folder yang telah dikompresi dengan nama `"kumpulan_$i"`. Opsi -r pada perintah rm menandakan bahwa folder yang dihapus berisi subfolder dan file-file di dalamnya. Berikut adalah code yang digunakan.
```bash
for(( i=1; i<=$foldernum; i++ ))
do
 zip -r $zipname kumpulan_$i
 rm -r kumpulan_$i
done'>forzipping.sh

chmod +x forzipping.sh
```
Setelah semua proses dijalankan, seluruh code akan dimasukkan kedalam `file forzipping.sh` dan diganti permissionnya dengan `chmod +x` agar bisa dieksekusi oleh seluruh _user_.

## **Soal Nomor 3**

Peter Griffin hendak membuat suatu sistem register pada script louis.sh dari setiap user yang berhasil didaftarkan di dalam file /users/users.txt. Peter Griffin juga membuat sistem login yang dibuat di script retep.sh

- **Register (louis.sh) :** <br>

Pertama, user akan diminta untuk memasukkan username dengan perintah:

```bash
read -p "Input username: " username
```

Selanjutnya, sistem akan mengecek terlebih dahulu apakah username yang dimasukkan sudah pernah didaftarkan atau belum. Jika belum pernah didaftarkan maka username berhasil didaftarkan dan masukkan username tersebut ke dalam users/users.txt agar tidak ada username yang sama di kedepannya:

```bash
while grep -q $username users/users.txt
do  echo "Username already exists, please enter new username!"
    echo "$(date +"%y/%m/%d %H:%M:%S") REGISTER: ERROR User already exists">>./log.txt
    read -p "Input username: " username
done
echo $username >> users/users.txt
```

`grep -q` digunakan untuk mencari apakah `$username` yang saat ini diinputkan sudah ada di dalam `users/users.txt` . `-q` berarti mennghilangkan tampilan output di layar.

Selanjutnya yaitu mengecek password apakah sudah sesuai dengan ketentuan yang diberikan:

```bash
read -sp "Input password: " password
```

opsi `-s` disini yaitu digunakan untuk membuat input menjadi hidden atau disembunyikan. sedangkan `-p` digunakan untuk menampilkan pesan "Input password:" pada layar.

**Minimal 8 karakter**

```bash
if [[ ! ${#password} -ge 8 ]];
```

**Memiliki minimal 1 huruf kapital dan 1 huruf kecil**

```bash
if [[ ! "$password" =~ [A-Z] ]];
if [[ ! "$password" =~ [a-z] ]];
```

**Alphanumeric**

```bash
if [[ "$password" =~ ^[a-zA-Z0-9]+$ ]];
```

**Tidak boleh sama dengan username**
<br>Sebelumnya untuk username yang berhasil didaftarkan akan disimpan di `users/users.txt`. Selanjutnya akan dibandingkan password yang sekarang dimasukkan dengan username yang dimasukkan tadi

```bash
if [[ "$password" == "$username" ]];
```

**Tidak boleh menggunakan kata chicken atau ernie**
```bash
if [[ "$password" == *ernie* || "$password" == *chicken* ]];
```

Selanjutnya yaitu mencatat setiap percobaan register pada log.txt dengan format : YY/MM/DD hh:mm:ss MESSAGE

```bash
echo "$(date +"%y/%m/%d %H:%M:%S") REGISTER: ERROR User already exists">>./log.txt

echo "$(date +"%y/%m/%d %H:%M:%S") REGISTER: INFO User $username registered successfully">> ./log.txt
```

Message pada log akan berbeda tergantung aksi yang dilakukan user.

Sehingga kode keseluruhan untuk sistem register akan seperti berikut:

```bash
#!/bin/bash

echo "REGISTER"
read -p "Input username: " username

while grep -q $username users/users.txt
do  echo "Username already exists, please enter new username!"
    echo "$(date +"%y/%m/%d %H:%M:%S") REGISTER: ERROR User already exists">>./log.txt
    read -p "Input username: " username
done
echo $username >> users/users.txt

wrongPassword=1
read -sp "Input password: " password
while [ $wrongPassword  == 1 ]
do  if [[ ! ${#password} -ge 8 ]];
    then
    echo "Your password should contain 8 characters or more!"
    wrongPassword=1
    read -sp "Input password: " password
    continue
    fi

    if [[ ! "$password" =~ [a-z] ]];
    then
		echo "Your password should contain at least one lowercase letter"
		wrongPassword=1
        read -sp "Input password: " password
        continue
	fi
	if [[ ! "$password" =~ [A-Z] ]];
    then
		echo "Your password should contain at least one uppercase letter"
		wrongPassword=1
        read -sp "Input password: " password
        continue
	fi
	if [[ "$password" == ^[a-zA-Z0-9]$ ]];
    then
		echo "Password must contain only alphanumeric"
		wrongPassword=1
        read -sp "Input password: " password
        continue
	fi

    if [[ "$password" == "$username" ]];
    then
        echo "Your password should not be the same as your username!"
        wrongPassword=1
        read -sp "Input password: " password
        continue
    fi

    if [[ "$password" == *ernie* || "$password" == *chicken* ]];
    then
        echo "Your password can't be 'ernie' or 'chicken'"
        wrongPassword=1
        read -sp "Input password: " password
        continue
    else
        wrongPassword=0
    fi
done
echo "Your account is successfully registered"
echo -e $username $password >>users/users.txt
echo "$(date +"%y/%m/%d %H:%M:%S") REGISTER: INFO User $username registered successfully">> ./log.txt
```

- **Login (retep.sh) :** <br>

Pertama, user akan diminta untuk memasukkan password dengan perintah:

```bash
read -p "Input username: " username
read -sp "Input password: " password
```

Selanjutnya, mengecek apakah username dan password tersebut pernah didaftarkan sebelumnya:

```bash
if grep -q "^$username $password$" users/users.txt;
```

Dan terakhir yaitu mencatat setiap percobaan login pada log.txt dengan format : YY/MM/DD hh:mm:ss MESSAGE

```bash
echo "$(date +"%y/%m/%d %T") LOGIN: INFO User $username logged in" >>./log.txt
echo "$(date +"%y/%m/%d %T") LOGIN: ERROR Failed login attempt on user $username" >>./log.txt
```

Message pada log akan berbeda tergantung aksi yang dilakukan user.

Sehingga kode keseluruhan untuk sistem login akan seperti berikut:

```bash
#!/bin/bash

echo -e "Anggota Kelompok A10 Praktikum Sistem Operasi\n"
echo -e "5025211043 - Anas Azhar"
echo -e "5025211080 - Dian Lies Seviona Dabukke"
echo -e "5025211188 - Akmal Ariq Romadhon"

echo "LOGIN"
read -p "Input username: " username
read -sp "Input password: " password

if grep -q "^$username $password$" users/users.txt;
then
	echo "$(date +"%y/%m/%d %T") LOGIN: INFO User $username logged in" >>./log.txt
	echo "Login Success"
else
	echo "$(date +"%y/%m/%d %T") LOGIN: ERROR Failed login attempt on user $username" >>./log.txt
	echo "Login Failed"
fi
```

## **Soal Nomor 4**

Johan Liebert adalah orang yang sangat kalkulatif. Oleh karena itu ia mengharuskan dirinya untuk mencatat log system komputernya. File syslog tersebut harus memiliki ketentuan :

- Backup file log system dengan format jam:menit tanggal:bulan:tahun (dalam format .txt)

Mengambil file log system dengan cara:

```bash
cat /var/log/syslog
```

Lalu menjadikan format nama untuk menyimpan backup file log system

```bash
filename=$(date "+%H":"%M %d":"%m":"%y.txt")
```

- Isi file harus dienkripsi dengan string manipulation yang disesuaikan dengan jam dilakukannya backup

Sebelum itu kita deklarasikan dulu variabel untuk menhimpan huruf dari a sampai z, baik huruf kapital maupun huruf kecil:

```bash
upper=(A B C D E F G H I J K L M N O P Q R S T U V W X Y Z)
lower=(a b c d e f g h i j k l m n o p q r s t u v w x y z)
```

Lalu untuk susunannya kita ubah berdasarkan jam saat ini. Misalkan jam 12 maka variabel upper dan lower diatas akan dirubah menjadi diawali dengan huruf m dan diakhiri dengan huruf l.

```bash
upper_cipher="${upper[$(date "+%H")]}-Z}A-${upper[$(date "+%H")-1]}"
lower_cipher="${lower[$(date "+%H")]}-z}a-${lower[$(date "+%H")-1]}"
```

Selanjutnya dilakukan enkripsi menggunakan `tr` yang berarti akan mentrasnlasikan huruf yang ada. Seperti `tr 'A-Z' 'M-Z}A-L'` maka akan mentranslasikan huruf A menjadi M, huruf B menjadi N, dan seterusnya.

```bash
echo -n "$(cat /var/log/syslog | tr 'A-Z' $upper_cipher | tr 'a-z' $lower_cipher)" > "$filename"
```

Sehingga keseluruhan kode untuk enkripsi akan tampak seperti berikut:

```bash
#/!bin/bash

filename=$(date "+%H":"%M %d":"%m":"%y.txt")

upper=(A B C D E F G H I J K L M N O P Q R S T U V W X Y Z)
lower=(a b c d e f g h i j k l m n o p q r s t u v w x y z)

upper_cipher="${upper[$(date "+%H")]}-Z}A-${upper[$(date "+%H")-1]}"
lower_cipher="${lower[$(date "+%H")]}-z}a-${lower[$(date "+%H")-1]}"

echo -n "$(cat /var/log/syslog | tr 'A-Z' $upper_cipher | tr 'a-z' $lower_cipher)" > "$filename"
```

- buat script untuk dekripsinya

Untuk dekripsi maka dilakukan kebalikan dari enkripsi. Jika tadi enkripsi menjalankan `tr 'A-Z' 'M-Z}A-L'`, maka di dekripsi akan menjalankan `tr 'M-Z}A-L' 'A-Z'`.

```bash
echo -n "$(cat "$fileencryptname" | tr $upper_cipher 'A-Z' | tr $lower_cipher 'a-z')" > "$filename"
```

Sehingga keseluruhan kode untuk dekripsi akan tampak seperti berikut:

```bash
#/!bin/bash

filename=$(date "+%H":"%M %d":"%m":"%y decrypt.txt")

upper=(A B C D E F G H I J K L M N O P Q R S T U V W X Y Z)
lower=(a b c d e f g h i j k l m n o p q r s t u v w x y z )

upper_cipher="${upper[$(date "+%H")]}-Z}A-${upper[$(date "+%H")-1]}"
lower_cipher="${lower[$(date "+%H")]}-z}a-${lower[$(date "+%H")-1]}"

echo -n "$(cat "$fileencryptname" | tr $upper_cipher 'A-Z' | tr $lower_cipher 'a-z')" > "$filename"
```

- Dan terakhir yaitu backup file syslog setiap 2 jam untuk dikumpulkan

```bash
crontab -l > backup
echo "0 */2 * * * /bin/bash /home/naz17/kuliah_sisop/sisop-praktikum-modul-1-2023-bj-a10/soal4/log_encrypt.sh" >> backup
crontab backup
```

# **_Screenshot_ Output**
Berikut adalah output dari tiap-tiap soal yang telah dikerjakan.

## **Soal 1**
Berikut adalah output dari soal nomor 1, yaitu 1A, 1B, 1C, dan 1D

![Soal1](https://media.discordapp.net/attachments/1083730715113426985/1083730778623574016/Screenshot_from_2023-03-10_19-07-46.png?width=527&height=616)

## **Soal 2**
- **Folder Zip yang telah dibuat.**

Berikut ialah ss output dari soal nomor 2, yang berisi Devil_$i.zip, kemudian terdapat file `fordownload.sh` dan `forzipping.sh` seperti code yang telah dijelaskan pada bagian sebelumnya.

![Soal02](https://media.discordapp.net/attachments/1083730715113426985/1083732173917528114/image.png?width=1095&height=616)

- **Crontab dari dua file.**

Berikut adalah output dari code yang dijalankan berupa _list_ dari crontab. Crontab ini berisi 2 perintah, yaitu menjalankan file `fordownload.sh` setiap 10 jam sekali, dan menjalankan file `forzipping.sh` tiap jam 00:00.

![Soal12](https://media.discordapp.net/attachments/1083730715113426985/1083734746674245792/Screenshot_from_2023-03-10_19-48-58.png?width=1095&height=616)

## **Soal 3**
- **Percobaan Registrasi**

Berikut merupakan percobaan Registrasi dan Login dari sistem yang sudah dibuat pada louis.sh dan retep.sh

![soal3](https://gitlab.com/azharanas173/sisop-praktikum-modul-1-2023-bj-a10/uploads/a031884c9cf6f0e5c15876b168d405d2/soal3.png)

## **Soal 4**
- **Proses Enkripsi dan Deskripsi**

Berikut merupakan percobaan menjalankan program unntuk enkripsi dan dekripsi dari log_encrypt.sh dan log_decrypt.sh

![soal41](https://gitlab.com/azharanas173/sisop-praktikum-modul-1-2023-bj-a10/uploads/560fa661111711455a20e073feff9fd4/soal41.png)

- **Output file yang dihasilkan**

Berikut adalah output file yang dihasilkan dari enkripsi dengan menjalankan crontab setiap 2 jam.

![soal42](https://gitlab.com/azharanas173/sisop-praktikum-modul-1-2023-bj-a10/uploads/560fa661111711455a20e073feff9fd4/soal41.png)

# **Kendala Selama Pengerjaan**
Berikut merupakan beberapa kendala dan kesulitan yang kami alami dalam pengerjaan Praktikum Modul 1 Sistem Operasi. Kami berharap kendala ini dapat diberbaiki di Praktikum selanjutnya.
- Ubuntu yang beberapa kali mengalami masalah, seperti cursor mengalami _freeze_ secara tiba-tiba dan keyboard tidak merespon saat ditekan.
- Masih awam dengan OS Linux dan _syntax_ yng ada, sehingga banyak proses _trial and error_ sebelum menyeleseikan praktikum.
- Kesusahan dalam implementasi kedalam code, padahal sudah mengerti perintah soal dan algoritma dari program yang ingin dibuat. Poin ini sebenarnya masih berhubungan dengan poin sebelumnya.
- Adanya beberapa soal yang terkesan ambigu dan menimbulkan beberapa pandangan dari sesama anggota kelompok. Salah satu contohnya ialah soal Nomor 1B yang beberapa kali mengalami perubahan.
- _Pressure_ dari kelompok lain yang sangat cepat dalam menyelesaikan praktikum, sehingga membuat beban mental.
- Cukup kesulitan untuk membuat kondisi dimana password harus hanya memuat alphanumeric saja.
- Sebelum demo password yang dimasukkan belum tersembunyi, kemudian dilakukan revisi dan sudah bisa tidak terlihat saat password dimasukkan.
- Cukup kesulitan untuk membuat sistem cipher yang bisa menerjemahkan huruf sesuai dengan jam saat program dijalankan, namun bisa terselesaikan dengan mencari referensi dari internet.
- Sebelum demo, program log_decrypt.sh harus diganti manual pada file yang akan didekripsikan. Namun setelah revisi sudah bisa tanpa diganti manual.
# **End of The Line**
```c
#include <stdio.h>

int main(){
    printf("Thank you!");
}
```
