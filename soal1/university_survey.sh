#!/bin/bash

echo -e "Anggota Kelompok A10 Praktikum Sistem Operasi\n"
echo -e "5025211043 - Anas Azhar"
echo -e "5025211080 - Dian Lies Seviona Dabukke"
echo -e "5025211188 - Akmal Ariq Romadhon"

echo -e "\nJawaban Nomor 1A :"
(awk -F, '$4 == "Japan" {print $1 " " $2}' 2023\ QS\ World\ University\ Rankings.csv | head -n 5)

echo -e "\nJawaban Nomor 1B :"
(awk -F, '$4 == "Japan" {print}' 2023\ QS\ World\ University\ Rankings.csv | sort -t, -g -k9 | head -n 5 | awk -F, '{print $2}')

echo -e "\nJawaban Nomor 1C :"
(awk -F, '$4 == "Japan" {print $2 "," $20}' 2023\ QS\ World\ University\ Rankings.csv | sort -t, -k2 -n | head -n 10 | awk -F, '{print $1 " " $2}')

echo -e "\nJawaban Nomor 1D :"
(awk -F, '/Keren/ {print $2}' 2023\ QS\ World\ University\ Rankings.csv)
