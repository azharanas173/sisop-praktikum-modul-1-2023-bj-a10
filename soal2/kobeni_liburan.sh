#!/bin/bash


cd $(pwd)
crontab -l > joblist
echo "0 */10 * * * cd $(pwd) && bash fordownload.sh" >> joblist
echo "0 0 * * * cd $(pwd) && bash forzipping.sh" >> joblist
crontab joblist

# Mencari nomor folder yang belum digunakan
echo -e '#!/bin/bash/
cd $(pwd)
i=1
while [ -d "kumpulan_$i" ]; do
    (( i++ ))
done

# Membuat folder baru dengan nama "kumpulan_N"
foldername="kumpulan_$i"
mkdir "$foldername"

# Mendapatkan jam sekarang
hour=$(date +%H)

# Menghitung jumlah download yang akan dilakukan (X)
if [ $hour -eq 0 ]; then
    X=1
else
    X=$hour
fi

# Loop untuk melakukan download sebanyak X kali
for (( j=1; j<=$X; j++ ))
do
    # Membuat nama file dengan format "perjalanan_NOMOR.FILE"
    filename="perjalanan_$j.jpg"

    # Melakukan download gambar dari URL yang diinginkan
    wget -O "$foldername/$filename" https://source.unsplash.com/1080x1920/?indonesia
done'>fordownload.sh

chmod +x fordownload.sh

echo -e '#!/bin/bash
cd $(pwd)
foldernum=$(ls -d kumpulan_* 2>/dev/null | wc -l)
zipfolder=$(ls -d Devil_* 2>/dev/null | wc -l)
zipname="Devil_$(($zipfolder+1)).zip"
for(( i=1; i<=$foldernum; i++ ))
do
 zip -r $zipname kumpulan_$i
 rm -r kumpulan_$i
done'>forzipping.sh

chmod +x forzipping.sh
