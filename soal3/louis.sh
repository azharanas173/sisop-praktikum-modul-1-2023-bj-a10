#!/bin/bash

echo -e "Anggota Kelompok A10 Praktikum Sistem Operasi\n"
echo -e "5025211043 - Anas Azhar"
echo -e "5025211080 - Dian Lies Seviona Dabukke"
echo -e "5025211188 - Akmal Ariq Romadhon\n"

echo "REGISTER"
read -p "Input username: " username

while grep -q $username users/users.txt
do  echo "Username already exists, please enter new username!"
    echo "$(date +"%y/%m/%d %H:%M:%S") REGISTER: ERROR User already exists">>./log.txt
    read -p "Input username: " username
done
echo $username >> users/users.txt

wrongPassword=1
read -sp "Input password: " password
while [ $wrongPassword  == 1 ]
do  if [[ ! ${#password} -ge 8 ]];
    then
    echo "Your password should contain 8 characters or more!"
    wrongPassword=1
    read -sp "Input password: " password
    continue
    fi

    if [[ ! "$password" =~ [a-z] ]]; 
    then
		echo "Your password should contain at least one lowercase letter"
		wrongPassword=1
        read -sp "Input password: " password
        continue
	fi
	if [[ ! "$password" =~ [A-Z] ]]; 
    then
		echo "Your password should contain at least one uppercase letter"
		wrongPassword=1
        read -sp "Input password: " password
        continue
	fi
	if [[ "$password" == ^[a-zA-Z0-9]+$ ]]; 
    then
		echo "Password must contain only alphanumeric"
		wrongPassword=1
        read -sp "Input password: " password
        continue
	fi

    if [[ "$password" == "$username" ]];
    then
        echo "Your password should not be the same as your username!"
        wrongPassword=1
        read -sp "Input password: " password
        continue
    fi

    if [[ "$password" == *ernie* || "$password" == *chicken* ]];
    then
        echo "Your password can't be 'ernie' or 'chicken'"
        wrongPassword=1
        read -sp "Input password: " password
        continue
    else
        wrongPassword=0
    fi
done
echo -e "\nYour account is successfully registered"
echo -e $username $password >>users/users.txt
echo "$(date +"%y/%m/%d %H:%M:%S") REGISTER: INFO User $username registered successfully">> ./log.txt