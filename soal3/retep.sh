#!/bin/bash

echo -e "Anggota Kelompok A10 Praktikum Sistem Operasi\n"
echo -e "5025211043 - Anas Azhar"
echo -e "5025211080 - Dian Lies Seviona Dabukke"
echo -e "5025211188 - Akmal Ariq Romadhon"

echo "LOGIN"
read -p "Input username: " username
read -sp "Input password: " password

if grep -q "^$username $password$" users/users.txt; 
then
	echo "$(date +"%y/%m/%d %T") LOGIN: INFO User $username logged in" >>./log.txt
	echo "Login Success"
else
	echo "$(date +"%y/%m/%d %T") LOGIN: ERROR Failed login attempt on user $username" >>./log.txt
	echo "Login Failed"
fi