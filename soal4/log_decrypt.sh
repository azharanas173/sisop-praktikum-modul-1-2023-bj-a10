#/!bin/bash

filename=$(date "+%H":"%M %d":"%m":"%y decrypt.txt")
fileencryptname=$(date "+%H":"%M %d":"%m":"%y.txt")

upper=(A B C D E F G H I J K L M N O P Q R S T U V W X Y Z)
lower=(a b c d e f g h i j k l m n o p q r s t u v w x y z )

upper_cipher="${upper[$(date "+%H")]}-Z}A-${upper[$(date "+%H")-1]}"
lower_cipher="${lower[$(date "+%H")]}-z}a-${lower[$(date "+%H")-1]}"

echo -n "$(cat "$fileencryptname" | tr $upper_cipher 'A-Z' | tr $lower_cipher 'a-z')" > "$filename"